# Startup Name Generator

A Flutter project.

*** FLUTTER *** 

L'application génère des propositions de noms de startup. 

A partir de cette liste, possibilité de sélectionner certains noms comma favoris. 

Une page recense la liste des nom favoris.

#### DESIGN DES VERSIONS

##### Version 1 - 06/04/2019

![Version 1](https://gitlab.com/claudebueno/startup_name_generator/raw/master/assets/images/sng-O1.png)